
# Configuration du grub

```bash
cp /etc/default/grub /etc/default/grub.bak  # Pour un backup 
vim /etc/default/grub
```

Dans l'édition il faut modifier les lignes :

- Pour le temp de selection des disques   
{- GRUB_TIMEOUT=10 -}  
{+ GRUB_TIMEOUT=3 +}

- Pour le disque chiffré
Exemple : cryptdevice=/dev/sda2:root  
{- GRUB_CMDLINE_LINUX= -}  
{+ GRUB_CMDLINE_LINUX="cryptdevice=/dev/tonDisque:TonDisqueMapper" +}

- Pour l'ancienne écriture du montage du disque  
{- #GRUB_DISABLE_LINUX_UUID=true -}  
{+ GRUB_DISABLE_LINUX_UUID=true +}

- Pour autoriser les disque chiffré  
{- #GRUB_ENBABLE_CRYPTODISK=y -}  
{+ GRUB_ENBABLE_CRYPTODISK=y +}

# Configuration sur l'image


```bash
cp /etc/mkinitcpio.conf /etc/mkinitcpio.conf.bak  # Pour un backup
vim /etc/mkinitcpio.conf
```

{- HOOKS=(base udev autodetect modconf block filesystems keyboard fsck) -}  
{+ HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck) +}

# Appliquer les modifications

```bash
mkinitcpio -p linux-lts
grub-mkconfig -o /boot/grub/grub.cfg
```

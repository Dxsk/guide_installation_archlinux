--> mtp

pacman -S gvfs-{afc,goa,google,gphoto2,mtp,nfs,smb}

--> gnome

pacman -S gdm gnome unoconv

--> kde 

pacman -S plasma kde-applications digikam elisa kdeconnect packagekit-qt5

--> xfce

pacman -S xfce4 xfce4-goodies gvfs vlc quodlibet python-pyinotify lightdm-gtk-greeter xarchiver claws-mail galculator evince ffmpegthumbnailer xscreensaver pavucontrol pulseaudio-{alsa,bluetooth} libcanberra-{pulse,gstreamer} system-config-printer

pacman -S lightdm-gtk-greeter-settings





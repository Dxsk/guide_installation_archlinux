<div align="center">
    <img src='./assets/archlinux-logo.png' />
</div>

# Guide d'installation d'un archlinux (WIP)
## Sommaire

- [**<ins>Étape 1</ins>** : Installation de la base](https://gitlab.com/Dxsk/archlinux_install/-/blob/master/live_install.md)
    - [**<ins>Étape 1.1</ins>** : Configurations du grub & de l'image avec disque chiffré](https://gitlab.com/Dxsk/archlinux_install/-/blob/master/config_linux_cryptsetup.md)
- [**<ins>Étape 2</ins>** : Après l'installation de base](https://gitlab.com/Dxsk/archlinux_install/-/blob/master/posts_install.md)

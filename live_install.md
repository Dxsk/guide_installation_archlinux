# Installation de base pour archlinux

## Avant l'installation

#### --> Si clavier français

```bash 
loadkeys fr
```

#### --> Pour se connecter en wifi

```bash
iwctl  # Entrée en mode configuration
device list  # Avoir la liste de ses cartes
station 'device' list 
station 'device' get-networks  # Avoir la listes des wifi
station 'device' connect 'SSID'  # Se connecter au wifi
```

#### --> Pour vérifier si vous avez une addresse ip lan

```bash
ip a  # Obtenir le status de ses cartes réseau
```

#### --> Si vous n'avez pas d'IP lan 

```bash 
dhcp 'device'  # Ont force le serveur DHCP à obtenir une IP lan
```

#### --> Pour vérifier que vous avez internet et que les requêtes DNS fonctionne.

```bash 
ping 1.1.1.1
ping google.com
```

### --> Mise à jour des mirroirs avec reflector

Prendre les 5 meilleurs mirroirs (dans le monde)
```bash
cp /etc/pacman.d/mirrorlist cp /etc/pacman.d/mirrorlist.bak  # Pour un backup
reflector --verbose -l 5 -p http --sort rate --save /etc/pacman.d/mirrorlist
```

#### --> Mise à jour de la liste des packets

```bash 
pacman -Syy
```

## Commencer l'installation

#### --> Création des partitions de base

Pour créer les partitions, plusieurs moyen sont possible suivant les besoins,
Là c'est l'exemple pour une création basique d'un disque avec un boot et une racine

```bash 
# for create a partition use cfdisk
# check your disk with fdisk -l 
# cfdisk /dev/'device'
# example: fcdisk /dev/sda
# create /dev/sda1 for boot/efi
# create /dev/sda2 for /
```

#### --> with crypto

```bash 
# warning to decrypt the disk later the keyboard will be in qwerty 
cryptsetup -y -v luksFormat /dev/sda2
cryptsetup open /dev/sda2 root
```

#### --> formate the partition

```bash 
# for the boot part
mkfs.vfat -F32 /dev/sda1

# with mapper device 
mkfs.ext4 /dev/mapper/root

# without mapper device
mkfs.ext4 /dev/sda2
```

#### --> mount

```bash 
mount /dev/mapper/root /mnt
mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot
```

#### --> base install system arch

```bash 
# La base du système
pacstrap /mnt base base-devel pacman-contrib

# Dans cette exemple j'utilise linux-lts mais rien n'empêche d'utiliser linux ou linux-hardened
# efibootmgr = c'est pour l'installation en mode EFI
# os-prober = c'est pour les multiboot (plusieurs installations style arch + windows par exemple)
pacstrap /mnt grub efibootmgr linux-lts linux-firmware linux-headers os-prober


# vous pouvez rajouter un éditeur de texte en CLI
# par exemple vim / neovim ou nano
pacstrap /mnt vim


# Des outils de base souvent utilisé
pacstrap /mnt zip unzip p7zip tar syslog-ng mtools dosfstools lsb-release ntfs-3g exfat-utils dhclient dhcpcd cronie ntp git

# Vous pouvez rajouter un nouveau shell 
pacstrap /mnt zsh

# Et pour la gestion du réseau et réseau wifi, il y a trois outils connus :
# networkManager qui gère le réseau sans wifi et le réseau n
pacstrap /mnt networkmanager
# ou
# pour le wifi 
pacstrap /mnt iwd
# pour le réseau ethernet ça sera avec systemd-networkd voir plus bas
```

#### --> and generate the FSTAB 

```bash 
genfstab -U -p /mnt >> /mnt/etc/fstab
```


## THE ARCH-CHROOT

Go to the chroot ! 

```bash
arch-chroot /mnt
```

#### More configs !

```bash
# if you use neovim ;)
ln -s /usr/bin/nvim /usr/bin/vim  
``` 

#### --> Config the locale

edit the locale.gen file 

```bash
# example : vim /etc/locale.gen
```

and change the line corresponding to the language you want to obtain on your distro

[- #fr_FR.UTF-8 -]  
[+ fr_FR.UTF-8 +]

```bash
# generate the locale file
locale-gen

# and exports the variable LANG with the chosen language
# example: export LANG=fr_FR.UTF-8
```

```bash
# configuration the clock, locale and lang

pacman -S ntp

# get the good localtime for your conf
# example : ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime

# if single os use this
hwclock --systohc --utc
```

### generate the kernel

```bash
mkinitcpio -p linux-lts
```


## Grub installation

#### --> for UEFI installation

```bash
mount | grep efivars &> /dev/null || mount -t efivarfs efivarfs /sys/firmware/efi/efivars
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=archlinux --recheck

mkdir -p /boot/EFI/boot
cp /boot/EFI/archlinux/grubx64.efi /boot/EFI/boot/bootx64.efi
```

### Config grub

```bash
grub-mkconfig -o /boot/grub/grub.cfg
```

### Config user and root
```bash
passwd root
groupadd youruser
useradd -m -g youruser -c "user infos" -s /bin/zsh youruser 
passwd youruser
```

## Plus de drivers 

### Drivers CPU
```bash
# driver for cpu intel
pacman -S intel-ucode

# driver for cpu amd
pacman -S amd-ucode

# driver for gpu intel 
pacman -S xf86-video-intel
```

### Drivers GPU

[check the wiki ;)](https://wiki.archlinux.fr/NVIDIA)

```bash
# driver for gpu nvidia (freedriver)
pacman -S xf86-video-nouveau

# driver for gpu nvidia (nonfreedriver)
pacman -S nvidia 
```

### Drivers de son

[Docs de pipewire](https://wiki.archlinux.org/title/PipeWire)

```bash
pacman -S pipewire pipewire-pulse pipewire-alsa pipewire-jack
```

### WebRTC screen sharing

```bash
pacman -S xdg-desktop-portal-gnome
pacman -S xdg-desktop-portal-kde
pacman -S xdg-desktop-portal-lxqt
pacman -S xdg-desktop-portal-wlr
```

```text 
This requires xdg-desktop-portal and one of its backends to be installed. The available backends are:
  - xdg-desktop-portal-gnome for GNOME, which depends on
  - xdg-desktop-portal-gtk for GTK applications.
  - xdg-desktop-portal-kde for KDE.
  - xdg-desktop-portal-lxqt for LXQt.
  - xdg-desktop-portal-wlr for wlroots-based Wayland compositors (e.g. Sway, dwlAUR)
```

### Divers pour les imprimentes 
```bash
pacman -S cups hplip python-pyqt5
pacman -S foomatic-{db,db-ppds,db-gutenprint,db-nonfree,db-nonfree-ppds} gutenprint
```

### Installation de font de police

```bash
pacman -S ttf-{bitstream-vera,liberation,freefont,dejavu} freetype2
```

### Installation des fichiers à la langue de l'utilisateur
```bash
pacman -S xdg-user-dirs
```

### Activé les services

```bash
# enable services
timedatectl set-ntp true
systemctl enable {syslog-ng@default,cronie,avahi-daemon,avahi-dnsconfd,ntpd,NetworkManager}
```
